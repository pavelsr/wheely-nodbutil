package WheeLy::CarLog;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::UserAgent;
use Mojo::DOM;
use Mojo::Message::Request;
use DBI;
use Array::Utils qw(:all);
use Data::Dumper;


# hash test at https://api.wheely.com/v3/cars?position=55.7258,37.6108
# https://api.wheely.com/v3/cars?id=52a0af8fb9a8832c7d000071
my $api_base = "https://api.wheely.com/v3/cars";


sub index {
  my $current_coordinates = "55.7258,37.6108";
  my $self = shift;
  my $result_for_location = Mojo::UserAgent->new;
  my $current_geo_json =   $result_for_location->get($api_base."?position=".$current_coordinates)->res->json;
  my $hash_ref_of_current_cars_for_coordinates = $current_geo_json->{'cars'};
  
  my @new_cars;
  for (keys $hash_ref_of_current_cars_for_coordinates) {
  	push (@new_cars, $_);
  }
  my $new_cars_number = @new_cars;
  my @known_cars = @ { show_known_cars () };
  my $known_cars_number = @known_cars;
  my @diff = array_minus(@new_cars, @known_cars); 
  my $unknown = @diff;
  my $last_id = update_db (@diff);
  my $last_time = get_time_by_upd_id ($last_id);
  my $current_time = current_time();
  my $time_diff = get_time_diff($last_time, $current_time);
  warn update_timing (@known_cars, $time_diff);

  $self->render(
  	result_for_location => $hash_ref_of_current_cars_for_coordinates,	# test hash output
  	new_cars_number => $new_cars_number,
  	cars_array => \@new_cars,
  	known_cars_number => $known_cars_number,
  	known_cars => \@known_cars,
	unknown => $unknown,
  	diff=> \@diff,
  	last_time => $last_time,
  	current_time => $current_time,
  	time_diff => $time_diff,
  	)
}




sub sql_server_connect {
	my $host = "localhost"; my $port = "3306"; my $user = "root"; my $pass = "0000"; my $db = "wheely";
	my $dbh = DBI->connect("DBI:mysql:$db:$host:$port",$user,$pass);
	return $dbh;
}

sub show_known_cars {
	my $dbh = sql_server_connect();
	my $array_ref = $dbh->selectcol_arrayref("SELECT car_id FROM cars");
	return $array_ref;
}

sub update_db {								# need check that value is unique
	my $dbh = sql_server_connect();
	for (@_) {
		my $sql_query = "INSERT INTO cars (car_id) VALUES ('".$_."');";
		my $sth = $dbh->prepare($sql_query);
		my $sql_status = $sth->execute();
	}
	my $sql_query2 = "INSERT INTO times (updated) VALUES ('".current_time()."');";
	my $sth = $dbh->prepare($sql_query2);
	my $sql_status = $sth->execute();
	my $id = $dbh->last_insert_id(undef, undef, "times", "updated");
	return $id;
}

sub update_timing {
	my ($cars, $time_diff) = @_;
	
	my $dbh = sql_server_connect();
	for (@$cars) {
		my @array = $dbh->selectrow_array("SELECT seconds_online FROM cars WHERE car_id = '$_'");
		my $online_secs = @array[0]+$time_diff;
		my $sql_query = "UPDATE cars SET seconds_online =".$online_secs." WHERE car_id = '$_'";
		my $sth = $dbh->prepare($sql_query);
		my $sql_status = $sth->execute();
	}
}

sub current_time {
        my($sec,$min,$hour,$mday,$mon,$year,$wday, $yday,$isdst)=localtime(time);
        my($result)=sprintf("%4d-%02d-%02d %02d:%02d:%02d",$year+1900,$mon+1,$mday,$hour,$min,$sec);
        return $result;
}

sub get_time_by_upd_id {
	my $id_to_update = @_[0];
	my $dbh = sql_server_connect();
	my @array = $dbh->selectrow_array("SELECT updated FROM times WHERE id = ".($id_to_update-1));
	use Data::Dumper;
	#warn Dumper(\@array);
	return @array[0];
}

sub get_time_diff {
	my ($past, $present) = @_;
	use Date::Parse;
	#my $str1 = "2014-02-24 11:42:37";
	#my $str2 = "2014-02-24 11:41:52";
	my $s1 = str2time($past);
	my $s2 = str2time($present);
	my $s3 = $s2-$s1;
	warn Dumper($s3);
	return $s3;
}


1;
